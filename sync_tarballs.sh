#!/usr/bin/env bash

for folder in ./software/* ; do
  pushd "${folder}" > /dev/null || exit
  package=$(basename "${folder}")

  echo "Doing ${package}"
  if [[ "$DRY_RUN" == "true" ]]; then
    flags='--no-download'
  fi

  latest_version=$(uscan -ddd --package "${package}" --watchfile watch --destdir "$(pwd)" --no-symlink ${flags} 2>&1 | grep -o "Newest version of ${package} on remote site is .*," | tr -d "," | awk '{print $NF}')

  if [[ "$DRY_RUN" == "true" ]]; then
    echo "  Latest version of ${package} is ${latest_version}"
    popd > /dev/null || exit
    continue
  fi

  echo "  Downloaded ${latest_version} of ${package}"

  package_file=$(find *.tar.*)

  upload_url="${CI_API_V4_URL}/projects/${CI_PROJECT_ID}/packages/generic/${package}/${latest_version}/${package_file}"

  curl -s -f -o /dev/null --header "JOB-TOKEN: $CI_JOB_TOKEN" --upload-file "${package_file}" "${upload_url}"

  echo "  Uploaded ${package_file} to ${upload_url}"

  popd > /dev/null || exit
done
