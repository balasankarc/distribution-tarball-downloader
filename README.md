# Distribution Tarball Downloader

Project to download tarballs of software components used in Distribution
projects and store them in GitLab's Generic Package Registry.

The project uses the
[uscan](https://manpages.debian.org/bullseye-backports/devscripts/uscan.1.en.html)
program, which is used by Debian project to fetch information about new releases
about the softwares included in it. This project stores
[watch](https://wiki.debian.org/debian/watch) files for software bundled in
GitLab distributions with information on fetching newer upstream versions of
them.

The watch file follows the following structure
```
version=4
opts="pgpmode=none" \
<URL where links to package tarballs are listed> @PACKAGE@@ANY_VERSION@@ARCHIVE_EXT@ 0.0
```

A periodic CI pipeline goes through this list of software, fetch the latest
versions of them, and pushes them to the [Generic package registry of this project](https://gitlab.com/balasankarc/distribution-tarball-downloader/-/packages).
